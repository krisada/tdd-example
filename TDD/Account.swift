//
//  Account.swift
//  TDD
//
//  Created by Developer on 11/19/14.
//  Copyright (c) 2014 Cycle. All rights reserved.
//

import UIKit

class Account: NSObject {
    lazy var balance = 0
    func deposit(amount:Int) {
        balance += amount
    }
    func withdraw(amount:Int) {
        if balance - amount >= 0 {
            balance -= amount
        }
    }
}
