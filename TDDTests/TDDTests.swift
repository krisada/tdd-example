//
//  TDDTests.swift
//  TDDTests
//
//  Created by Developer on 11/19/14.
//  Copyright (c) 2014 Cycle. All rights reserved.
//

import UIKit
import XCTest

class TDDTests: XCTestCase {
    var account:Account!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        account = Account()

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    func testInit() {
        XCTAssertEqual(account.balance, 0, "Account Not Set To Zero.")
    }
    func testDepositAndWithdraw() {
        // This is an example of a functional test case.
        account.deposit(50)
        XCTAssertEqual(account.balance, 50, "Can't Deposit Account.")
        account.withdraw(20)
        XCTAssertEqual(account.balance, 30, "Can't Withdraw Account.")
    }
    func testExtraWithdraw() {
        account.withdraw(20)
        XCTAssertEqual(account.balance, 0, "Can't Withdraw Account.")
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock() {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
